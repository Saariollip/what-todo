import React from 'react';
//import { Platform } from 'react-native';
import {
  ScrollView,
  StyleSheet,
  View,
  Text,
  Linking,
  TouchableOpacity } from 'react-native';
import { Button, CheckBox, Icon } from 'react-native-elements'
import TabBarIcon from '../components/TabBarIcon';

const leftContent = <Text>ICON <TabBarIcon name={'md-heart'}/></Text>

export default class LinksScreen extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      opts: [{
        title: "Environment",
        color: "#00a15d",
        choises: [
          {
            icon: "home", 
            name: 'Indoors',
            checked: false,
            disabled: false
          },
          {
            icon: 'public',
            name: 'Outdoors',
            checked: false,
            disabled: false
          }
        ]
      },
      {
        title: "Company",
        color: "#286d8c",
        choises: [
          {
            icon: "person",
            name: 'Alone',
            checked: false,
            disabled: false
          },
          {
            icon: "supervisor-account",
            name: 'With company',
            checked: false,
            disabled: false
          }
        ]
      },
      {
        title: "Activity",
        color: "#8c4aa4",
        choises: [
          {
            icon: 'videogame-asset',
            name: 'Relax',
            checked: false,
            disabled: false
          },
          {
            icon: "book",
            name: 'Intellectual',
            checked: false,
            disabled: false
          },
          {
            icon: "directions-walk",
            name: 'Exercise',
            checked: false,
            disabled: false
          }
        ]
      },
      {
        title: "Cost",
        color: "#00a29f",
        choises: [
          {
            icon: "money-off",
            name: 'Free',
            checked: false,
            disabled: false
          },
          {
            icon: "credit-card",
            name: 'Paid',
            checked: false,
            disabled: false
          }
        ]
      }],
      text: '',
      placeholder: "Enter city here",
      items: [],
      isVisible: false,
      selectedIndex: 0,
      result: "",
      link: null
    };
    this.updateIndex = this.updateIndex.bind(this)
    this._getAnswer = this._getAnswer.bind(this);
  }

  /*
  async componentDidMount() {
    await this._searchApartments("oulu")
  }
  */
  static navigationOptions = {
    title: 'Bored?',
    //header: null,
    headerStyle: {
      textAlign: "center",
      backgroundColor: '#4caf50',
    },
    /*
    header: {
      titleStyle: {
        textAlign: "center"
      }
    },
    */
   titleStyle: {
     textAlign: 'center'
   },
    headerTintColor: '#fff',
    headerTitleStyle: {
      fontWeight: 'bold',
      textAlign: "center"
    },
  };

  render() {
    const { selectedIndex } = this.state
    //{this.state.result.length ? styles.result : styles.container}
    return (
      <ScrollView style={styles.container}>
        <View style={{flex: 1, flexWrap: "wrap", flexDirection: "row"}}>
        {
          this.state.opts.map((opt, i) => {
            return (
              <View style={styles.optContainer} key={i}>
                <Text style={styles.titleText} key={i}> {opt.title}</Text>
                <View style={styles.viewContainer} key={(i+1)}>
                {opt.choises.map((item, j) => {
                const k = (i+j+1);
                  return (
                      <View key={k} style={styles.checkboxContainer}>
                       <TouchableOpacity
                            onPress={() => {
                              this.setState((prev) => {
                                prev.opts[i].choises[j].checked = !prev.opts[i].choises[j].checked;
                                const choise = prev.opts[i].choises[j];
                                if (prev.opts[i].choises.length < 3) {
                                  if (j === 0) {
                                    prev.opts[i].choises[1].checked = false;
                                    prev.opts[i].choises[1].disabled = prev.opts[i].color.concat("80");
                                    prev.opts[i].choises[0].disabled = false;
                                  } else {
                                    prev.opts[i].choises[0].checked = false;
                                    prev.opts[i].choises[0].disabled = prev.opts[i].color.concat("80");
                                    prev.opts[i].choises[1].disabled = false;
                                  }
                                  if (!prev.opts[i].choises[0].checked && !prev.opts[i].choises[1].checked) {
                                    prev.opts[i].choises[0].disabled = false;
                                    prev.opts[i].choises[1].disabled = false;
                                  }
                                } else {


                                }
                                const newChoise = !choise.checked;
                                return {...choise, checked: newChoise}
                              });
                            }}
                            style={[styles.touchable, {backgroundColor: this.state.opts[i].choises[j].disabled || this.state.opts[i].color}]}
                          >
                          <Icon name={this.state.opts[i].choises[j].icon} size={30} color="#ffffff" />
                        </TouchableOpacity>
                        <Text> {this.state.opts[i].choises[j].name}</Text>
    
                      </View>
                  );
                })
              }
              </View>
            </View>)
          })
        }
        </View>
          <View style={styles.buttonContainer}>
            <TouchableOpacity
              style={styles.find}
              containerStyle={styles.buttonContainer}
              buttonStyle={styles.findButton}
              onPress={this._getAnswer}
              title="Find me something to do!"
              color="#ffffff"
              accessibilityLabel="Tell me something to do"
            >
            <Text>Find</Text>
            </TouchableOpacity>
          </View>
          <View>
            <Text style={styles.titleText}>{this.state.result}</Text>
            { this.state.link &&
              <Text style={{color: 'blue'}}
                  onPress={() => Linking.openURL(this.state.link)}>
              Check here
            </Text>}
          </View>
      </ScrollView>
    );
  }

  updateIndex(selectedIndex) {
    this.setState({selectedIndex});
  }

  async _getAnswer() {
    const params = this.state.opts;
    const query = {};
    for (let i = 0; i < params.length; i++) {
      for (let j = 0; j < params[i].choises.length; j++) {
        if (params[i].choises[j].checked === true) {
          if (params[i].hasOwnProperty('title')) {
            if (!query[params[i].title.toLowerCase()]) query[params[i].title.toLowerCase()] = [];
            query[params[i].title.toLowerCase()].push(params[i].choises[j].name);
          }
        }
      }
    }
    const searchUrl = `https://find-apartment.herokuapp.com/api/get_todo?${serialize(query).toLowerCase()}`;
    try {
      const response = await fetch(searchUrl);
      const responseJson = await response.json();
      this.setState(previousState => (
        { result: ucFirst(responseJson.idea), link: responseJson.link }
      ))
      return responseJson;
    } catch (error) {
      return {}
    }
  }

  _getIndoorInfo = (a, b) => {
    console.log("a: ", a);
    console.log("b: ", b);
  }

  _getGeoLocation() {
      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(
          (position) => {
            this.setState({
              latitude: position.coords.latitude,
              longitude: position.coords.longitude,
              error: null,
            });
          },
          (error) => this.setState({ error: error.message }),
          { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 },
        );
      } else { 
        console.error("Geolocation is not supported by this Mobile OS.");
      }
  }

  _searchApartments = async (searchTerm = null) => {
    if (!searchTerm) {
      searchTerm = this.state.text;
    }
    if (!searchTerm.length) return false;
    searchTerm = searchTerm.toLowerCase();
    const searchUrl = `https://find-apartment.herokuapp.com/api/get_rent_data?q=${searchTerm}`
    try {
      const response = await fetch(searchUrl);
      let responseJson = response.json();
      this.setState(previousState => (
        { items: responseJson.items }
      ))
      return responseJson.items;
    } catch (error) {
      console.error(error);
    }
    return [];
  };

  _showOptions = (visible) => {
    this.setState({isVisible: !this.state.isVisible});
  }

  _goToApartmentLink = async(index) => {
    const link = this.state.items[index].link
    return WebBrowser.openBrowserAsync(link);
  };
};


const ucFirst = string => string.charAt(0).toUpperCase() + string.slice(1);

const serialize = (obj) => {
  var str = [];
  for (var p in obj)
    if (obj.hasOwnProperty(p)) {
      str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
    }
  return str.join("&");
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#ffffff"
    //backgroundColor: '#ebedf1',
  },
  optContainer: {
    marginBottom: 20,
    marginRight: 10,
  //  borderRadius: 4,
    borderBottomWidth: 0.5,
    borderColor: '#d6d7da',
    paddingTop: 15
  },
  result: {
    flex: 1,
    backgroundColor: "#90EE90"
  },
  viewContainer: {
    flex: 1,
    marginLeft: 0,
    marginRight: 0,
    flexDirection: "row"
  },
  checkboxContainer: {
    padding: 5
  },
  checkbox: {
    borderRadius: 50,
    backgroundColor: "#398AD7",
    padding: 0
  },
  titleText: {
    fontSize: 22,
  },
  checkboxText: {
    color: "#fff",
    padding: 10
  },
  buttonContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    /*backgroundColor: "#2F66A9",*/
    marginTop: 10
  },
  findButton: {
    backgroundColor: "#2F66A9",
    flex: 3,
    padding: 10
  },
  touched: {
    borderWidth:1,
    borderColor:'rgba(0,0,0,0.5)',
    alignItems:'center',
    justifyContent:'center',
    width:70,
    height:70,
    backgroundColor:'#398AD7',
    borderRadius:100,
  },
  touchable: {
    borderWidth:1,
    borderColor:'rgba(0,0,0,0.2)',
    alignItems:'center',
    justifyContent:'center',
    width:70,
    height:70,
    backgroundColor:'#fff',
    borderRadius:100,
  },
  find: {
    borderWidth:1,
    borderColor:'rgba(0,0,0,0.2)',
    alignItems:'center',
    justifyContent:'center',
    width:100,
    height:100,
    backgroundColor:'#fff',
    borderRadius:100,
  }
});
