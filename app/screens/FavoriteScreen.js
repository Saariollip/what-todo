import React from 'react';
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View
} from 'react-native';

import { Card, ListItem, Button, Icon } from 'react-native-elements'
import { WebBrowser } from 'expo';

import { MonoText } from '../components/StyledText';

export default class HomeScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {items: [] };
    //this.probs = { items: []}
  }

  /*
  static navigationOptions = {
    title: 'Favorite apartments',
    //header: null,
    headerStyle: {
      backgroundColor: '#e91e63',
    },
    headerTintColor: '#fff',
    headerTitleStyle: {
      fontWeight: 'bold',
    },
  };
*/

  render() {
    return (
      <ScrollView style={styles.container}>
        <Text style={styles.titleText}>Bored?</Text>
        <View style={styles.buttonContainer}>
          <TouchableOpacity
              style={{
                  borderWidth:1,
                  borderColor:'rgba(0,0,0,0.2)',
                  alignItems:'center',
                  justifyContent:'center',
                  width:100,
                  height:100,
                  backgroundColor:'#fff',
                  borderRadius:100,
                }}
            >
            <Icon name={"chevron-right"} size={30} color="#01a699" />
          </TouchableOpacity>
        </View>
        <View>
          <Text style={styles.titleText}>
            {'\n'}{this.state.result}{'\n'}
          </Text>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  developmentModeText: {
    marginBottom: 20,
    color: 'rgba(0,0,0,0.4)',
    fontSize: 14,
    lineHeight: 19,
    textAlign: 'center',
  },
  contentContainer: {
    paddingTop: 30,
  },
  welcomeContainer: {
    alignItems: 'center',
    marginTop: 10,
    marginBottom: 20,
  },
  welcomeImage: {
    width: 100,
    height: 80,
    resizeMode: 'contain',
    marginTop: 3,
    marginLeft: -10,
  },
  getStartedContainer: {
    alignItems: 'center',
    marginHorizontal: 50,
  },
  homeScreenFilename: {
    marginVertical: 7,
  },
  codeHighlightText: {
    color: 'rgba(96,100,109, 0.8)',
  },
  codeHighlightContainer: {
    backgroundColor: 'rgba(0,0,0,0.05)',
    borderRadius: 3,
    paddingHorizontal: 4,
  },
  getStartedText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    lineHeight: 24,
    textAlign: 'center',
  },
  tabBarInfoContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    ...Platform.select({
      ios: {
        shadowColor: 'black',
        shadowOffset: { height: -3 },
        shadowOpacity: 0.1,
        shadowRadius: 3,
      },
      android: {
        elevation: 20,
      },
    }),
    alignItems: 'center',
    backgroundColor: '#fbfbfb',
    paddingVertical: 20,
  },
  tabBarInfoText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    textAlign: 'center',
  },
  navigationFilename: {
    marginTop: 5,
  },
  helpContainer: {
    marginTop: 15,
    alignItems: 'center',
  },
  helpLink: {
    paddingVertical: 15,
  },
  helpLinkText: {
    fontSize: 14,
    color: '#2e78b7',
  },
  container: {
    flex: 1,
    backgroundColor: "#fff"
    //backgroundColor: '#ebedf1',
  },
  titleText: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  viewContainer: {
    flex: 1,
    backgroundColor: "#ddd",
    marginLeft: 0,
    marginRight: 0,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  checkboxContainer: {
    flex: 1,
    padding: 5
  },
  checkbox: {
    borderRadius: 50,
    backgroundColor: "#398AD7",
    padding: 0
  },
  titleText: {
    fontSize: 24,
    textAlign: "center"
  },
  checkboxText: {
    color: "#fff",
    padding: 10
  },
  buttonContainer: {
    flex: 1,
    backgroundColor: "#2F66A9",
    marginTop: 30
  },
  findButton: {
    backgroundColor: "#2F66A9",
    flex: 3,
    padding: 10
  }
});